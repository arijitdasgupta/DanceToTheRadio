import axios from 'axios';

export const loadInitialData = (dispatch) => {
    axios.get('/rest').then(resp => {
        dispatch({
            type: 'LOAD_INITIAL',
            payload: {
                columns: resp.data.points.filter((item) => item.type === 'column'),
                rows: resp.data.points.filter((item) => item.type === 'row'),
                question: resp.data.questionData
            }
        });
    });
}

export const setQuestion = (questionString) => (dispatch) => {
    dispatch({
        type: 'SET_LOADING'
    });
    axios.put('/rest/question', { label: questionString }).then(() => {
        loadInitialData(dispatch);
    });
}

export const addPoint = (pointType) => (dispatch) => {
    dispatch({
        type: 'SET_LOADING'
    });
    axios.post('/rest/point', {
        type: pointType,
        label: null
    }).then((data) => {
        loadInitialData(dispatch);
    });
}

export const editPoint = (pointId, pointLabel) => (dispatch) => {
    dispatch({
        type: 'SET_LOADING'
    });
    axios.put(`/rest/point/${pointId}`, {
        label: pointLabel
    }).then(() => {
        loadInitialData(dispatch);
    });
}

export const removePoint = (pointId) => (dispatch) => {
    dispatch({
        type: 'SET_LOADING'
    });
    axios.delete(`/rest/point/${pointId}`).then(() => {
        loadInitialData(dispatch);
    });
}

export const addImage = (pointId, imageData, contentMimeType) => (dispatch) => {
    dispatch({
        type: 'SET_LOADING'
    });
    axios.post(`/images/${pointId}`, imageData, {
        headers: {
            'Content-Type': contentMimeType
        }
    }).then(() => {
        loadInitialData(dispatch);
    });
}

//TODO: Add image...