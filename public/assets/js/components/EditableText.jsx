import React from 'react';
import {compose} from 'redux';

export class EditableText extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            editingText: false
        };
    }

    toggleEdit() {
        this.setState({
            editingText: !this.state.editingText
        });
    }

    handleChange(event) {
        const questionTarget = event.target.value;
        event.stopPropagation();
        if (event.keyCode === 13 && questionTarget.trim().length !== 0) {
            this.setState({
                editingText: false
            });
            this.props.setText(questionTarget);
        } else if (event.keyCode === 27) {
            this.setState({
                editingText: false
            });
        }
    }

    setChange(event) {
        event.stopPropagation();
        const questionTarget = event.target.value;
        if (questionTarget.trim().length !== 0) {
            this.props.setText(event.target.value);
        }
        this.setState({
            editingText: false
        });
    }

    render() {
        return (this.state.editingText ? 
            <input 
                className={this.props.className}
                onKeyUp={this.handleChange.bind(this)} 
                onBlur={this.setChange.bind(this)}
                type='text' /> :
            <p className={this.props.className} onClick={this.toggleEdit.bind(this)}>{this.props.text}</p>);
    }
}