import React from 'react';

import {EditableText} from './EditableText.jsx';

const AddButton = (props) => {
    return <button className='radioTable__addButton radioTable__button' onClick={props.onClick}>+</button>;
}

const RemoveButton = (props) => {
    return <button className='radioTable__removeButton radioTable__button' onClick={props.onClick}>-</button>;
}

const AddImageButton = (props) => {
    return <div className='radioTable__fileInput'>
        <button className='radioTable__fileInputButton radioTable__button'>+</button>
        <input className='radioTable__fileInputElement' type='file' multiple={false} onChange={props.onChange} />
    </div>;
}

const ImageThumbnail = (props) => {
    return <div className='radioTable__fileInput'>
        <img className='radioTable__thumbnail' src={props.src} />
        <input className='radioTable__fileInputElement' type='file' multiple={false} onChange={props.onChange} />
    </div>;
}

export class Table extends React.Component {
    constructor(props, context) {
        super(props, context);
    }

    addRow() {
        this.props.addPoint('row');
    }

    addColumn() {
        this.props.addPoint('column');
    }

    removePoint(pointId) {
        return () => {
            this.props.removePoint(pointId);
        };
    }

    editPoint(pointId) {
        return (pointLabel) => {
            this.props.editPoint(pointId, pointLabel);
        };
    } 

    // TODO: Refactor...
    addImage(pointId) {
        return (event) => {
            const file = event.target.files[0];
            const reader = new FileReader();
            reader.onload = (e) => {
                this.props.addImage(pointId, reader.result, file.type);        
            }
            reader.readAsArrayBuffer(event.target.files[0]);
        };
    }

    // Janky, needs refactor! TODO
    render() {
        return (<table className='radioTable__mainTable'>
            <tbody>
                <tr>
                    <td></td>
                    {this.props.rows.length > 0 ? <td></td> : null}
                    {this.props.columns.map(column => <td key={column._id}>
                        { column.hasImage ? 
                            <ImageThumbnail key={Math.random()} src={`/images/${column._id}`} onChange={this.addImage(column._id).bind(this)} /> : 
                            <AddImageButton onChange={this.addImage(column._id).bind(this)} />}
                    </td>)}
                    <td>
                        <AddButton onClick={this.addColumn.bind(this)} />
                    </td>
                </tr>
                <tr>
                    <td></td>
                    { this.props.rows.length > 0 ? <td></td> : null }
                    {this.props.columns.map((column, index) => <td key={column._id}><EditableText 
                        setText={this.editPoint(column._id).bind(this)} 
                        text={column.label ? column.label : `Col${index + 1}`} /></td>
                    )}
                </tr>
                {this.props.rows.map((row, index) => {
                    return <tr key={row._id}>
                        <td>
                            { row.hasImage ? 
                                <ImageThumbnail key={Math.random()} src={`/images/${row._id}`} onChange={this.addImage(row._id).bind(this)} /> : 
                                <AddImageButton onChange={this.addImage(row._id).bind(this)} />}
                            
                        </td>
                        <td>
                            <EditableText
                                setText={this.editPoint(row._id).bind(this)}
                                text={row.label ? row.label : `Row${index + 1}`} />
                        </td>
                        {this.props.columns.map(column => <td key={column._id}><input type='radio' /></td>)}
                        <td><RemoveButton 
                            onClick={this.removePoint(row._id).bind(this)} />
                        </td>
                    </tr>
                })}
                <tr>
                    <td>
                        <AddButton onClick={this.addRow.bind(this)} />
                    </td>
                    {this.props.rows.length > 0 ? <td></td> : null}
                    {this.props.columns.map(column => <td key = {column._id}><RemoveButton 
                        onClick={this.removePoint(column._id).bind(this)} /></td>)}
                </tr>
            </tbody>
        </table>);
    }
}