import React from 'react';
import {connect} from 'react-redux';

class StatsClass extends React.Component {
    constructor(props, context) {
        super(props, context);
    }

    render() {
        const nOfImages = this.props.state.rows.concat(this.props.state.columns).filter(item => item.hasImage).length;
        const longestRowLabel = Math.max.apply(this, this.props.state.rows.map(item => item.label ? item.label.length : 0));
        const longestColumnLabel = Math.max.apply(this, this.props.state.columns.map(item => item.label ? item.label.length : 0));

        return (<div className='radioTable__stats padded'>
            <p>Number of rows: {this.props.state.rows.length}</p>
            <p>Number of columns: {this.props.state.columns.length}</p>
            <p>Number of images uploaed: {nOfImages}</p>
            <p>Longest column label: {longestColumnLabel}</p>
            <p>Longest row label: {longestRowLabel}</p>
            <br/>
            <p>To add images per row/column, click on the corresponding "+" button</p>
            <p>To add row/column, click on the "+" buttons at the corners</p>
            <p>Click on the question title or the column and row names to edit them, press ENTER or defocus to save. Escape to cancel</p>
            <p>You can click on an image to re-upload a different image</p>
            <p>Keep image size below 2mbs</p>
            { this.props.state.loading ? <p>Loading...</p> : null }
        </div>);
    }
}

export const Stats = connect((state) => {
    return {state}
})(StatsClass);