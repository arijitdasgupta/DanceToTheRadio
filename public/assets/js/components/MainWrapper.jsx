import React from 'react';
import {connect} from 'react-redux';
import {compose} from 'redux';

import {Stats} from './Stats.jsx';
import {RadioTable} from './RadioTable.jsx';
import {loadInitialData} from '../actions/action.js';
import {EditableText} from './EditableText.jsx';
import {setQuestion} from '../actions/action.js';

class MainWrapperClass extends React.Component {
    constructor() {
        super();
    }

    componentDidMount() {
        this.props.loadInitial();
    }

    render() {
        return (<div>
            <h1>"...dance, to the radio" - <i>Joy Division, Transmission</i></h1>
            <EditableText 
                className='radioTable__questionLabel'
                text={this.props.state.question.label}
                setText={this.props.setQuestion} />
            <div className='wrapper'>
                <RadioTable />
                <Stats />
            </div>
        </div>);
    }
}

export const MainWrapper = connect((state) => ({state}), (dispatch) => ({
    loadInitial: () => dispatch(loadInitialData),
    setQuestion: compose(dispatch, setQuestion)
}))(MainWrapperClass);
