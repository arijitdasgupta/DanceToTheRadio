import React from 'react';
import {connect} from 'react-redux';
import {compose} from 'redux';

import {setQuestion, addPoint, editPoint, removePoint, addImage} from '../actions/action.js';
import {EditableText} from './EditableText.jsx';
import {Table} from './Table.jsx';

class RadioTableClass extends React.Component {
    constructor(props, context) {
        super(props, context);
    }

    render() {
        return <div className='radioTable padded'>
            <Table 
                rows = {this.props.state.rows} 
                columns = {this.props.state.columns} 
                addPoint = {this.props.addPoint} 
                removePoint = {this.props.removePoint} 
                editPoint = {this.props.editPoint} 
                addImage = {this.props.addImage} />
        </div>;
    }
}

export const RadioTable = connect((state) => ({state}), (dispatch) => ({
    addPoint: compose(dispatch, addPoint),
    removePoint: compose(dispatch, removePoint),
    editPoint: compose(dispatch, editPoint),
    addImage: compose(dispatch, addImage)
}))(RadioTableClass);