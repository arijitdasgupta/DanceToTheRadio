import React from 'react';
import {createStore, applyMiddleware} from 'redux';
import {Provider, connect} from 'react-redux';
import logger from 'redux-logger';
import thunk from 'redux-thunk';

import {reducer} from './reducers/reducer.js';
import {MainWrapper} from './components/MainWrapper.jsx';

export class Application extends React.Component {
    constructor(props, context) {
        super(props, context);

        this._store = createStore(reducer, { // This can go somewhere else
            rows: [],
            columns: [],
            question: {
                label: 'Set a question label'
            },
            loading: true
        }, applyMiddleware(logger, thunk));
    }

    render() {
        return (<Provider store={this._store}>
            <MainWrapper />
        </Provider>);
    };
}