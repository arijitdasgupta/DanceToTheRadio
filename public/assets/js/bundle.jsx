import React from 'react';
import ReactDOM from 'react-dom';

import mainCss from '../styles/main.scss';
import { Application } from './Application.jsx';

const initApplication = () => {
    const mainAppContainer = document.getElementById('main-application');
    ReactDOM.render(<Application />, mainAppContainer);
}

initApplication();