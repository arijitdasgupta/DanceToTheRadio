const cloneDeep = (object) => JSON.parse(JSON.stringify(object));

export const reducer = (state, action) => {
    switch (action.type) {
        case 'LOAD_INITIAL':
            state.rows = action.payload.rows;
            state.columns = action.payload.columns;
            // Question can be null...
            if (action.payload.question) {
                state.question = action.payload.question;
            }
            state.loading = false;
            return cloneDeep(state);
        case 'SET_LOADING': 
            state.loading = true;
            return cloneDeep(state);
        default:
            return state;
    }
}