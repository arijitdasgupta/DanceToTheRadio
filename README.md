## Persistent storage simple radio button editor...

This is a full-stack simple application capable of editing a matrix radio buttons, and storing images and labels corresponding to the rows and columnes.

The application uses Amazon S3 compatible object storage to serve and store images. Also uses MongoDB to store other relevant information such as labels, image flags etc. Manages both the CRUD operations for the S3 buckets and the MongoDB documents automatically.

### Pre-requisites to run

 - Recommended node version v7.0.0 or up.
 - Amazon S3 compatible object storage instance.
 - Running MongoDB instance, with collection name `points` and `question` un-used.

### Environment variables
```bash
# Amazon S3 Endpoint
AMAZON_S3_ENDPOINT
# Amazon S3 Key
AMAZON_S3_KEY
# Amazon S3 Secret Key
AMAZON_S3_SECRET
# Node Port to run the service on
NODE_PORT
# Mongo conneciton string with protocol string
MONGO_CONNECTION_STRING
```

### Pre-requisites
```bash
npm install -g yarn # if you don't have yarn already
```

### To run the application
```bash
yarn install
yarn build
yarn start
```

### For developement
```bash
yarn watch:frontend
```

```bash
yarn watch:backend
```

By default the application runs on `PORT 3000`. When using development mode, it runs on `PORT 8080` by default.

### NOTES: 
 - No production build for frontend
 - No proper REST API Response code
 - No retries in API or in frontend
 - No label is automatically labelled, if no label it's auto labelled only on the view layer
 - The table component needs refactor
 - No proper loading indicator
 - No separate dev dependency
 - No unit tests
 - Blank text entry validation is only in front-end
 - No Gzipping of images etc.
