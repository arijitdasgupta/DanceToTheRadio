FROM node:7.0.0

RUN ["mkdir", "app"]
COPY . app/
WORKDIR app
RUN npm install -g yarn
RUN ["yarn", "install"]
RUN ["yarn", "run", "build"]

CMD ["yarn", "start"]