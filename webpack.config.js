module.exports = {
    entry: "./public/assets/js/bundle.jsx",
    output: {
        filename: "bundle.js",
        path: __dirname + "/build",
        publicPath: "/"
    },

    // Enable sourcemaps for debugging webpack's output.
    devtool: "source-map",

    // Dev Server
    devServer: {
        inline: true,
        proxy: {
            '/rest': {
                target: 'http://localhost:3000',
                secure: false
            },
            '/images': {
                target: 'http://localhost:3000',
                secure: false
            }
        }
    },

    module: {
        loaders: [
            {
                test : /\.jsx?/,
                loader : 'babel-loader'
            },
            { test: /\.scss$/, use: [{
                    loader: "style-loader"
                }, {
                    loader: "css-loader"
                }, {
                    loader: "sass-loader",
                }]
            },
            {
                test: /\.(png|json)$/,
                use: [{loader: 'file-loader'}]
            }
        ]
    }
};
