const AWS = require('aws-sdk');
const mongo = require('mongodb');
const process = require('process');

const createMainApplication = require('./mainApplication');

/**
 * Env:
 * S3 Endpoint:
 * accessKeyId:
 * secretAccessKey:
 * Service Port:
 * MongoDB connection string:
 */

const PORT = parseInt(process.env.NODE_PORT, 10) || 3000;

const mongoConnectionString = process.env.MONGO_CONNECTION_STRING || 'mongodb://localhost:27017';

mongo.MongoClient.connect(mongoConnectionString).then((mongoConnection) => {
    // Initiating S3 object
    const s3 = new AWS.S3({
        endpoint: process.env.AMAZON_S3_ENDPOINT || 'http://localhost:1234',
        accessKeyId: process.env.AMAZON_S3_KEY || '24534645DHGHDDGS',
        secretAccessKey: process.env.AMAZON_S3_SECRET || '248578425adcsdfuiwh4u4y28y5824yht473g35ti',
        apiVersion: '2006-03-01'
    });

    const app = createMainApplication(mongoConnection, s3);
    console.log(`Application started on ${PORT}`);
    app.listen(PORT);
}).catch((err) => {
    console.log('Failed to establish MongoDB connection', err);
});
    