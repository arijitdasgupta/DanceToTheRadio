const express = require('express');
const bodyParser = require('body-parser');

class RestApplication { 
    constructor(dataService) {
        this._dataService = dataService;
        this._application = new express();
        this._application.use(bodyParser.json());
        this._application.get('/', this._getAllPointsAndData.bind(this));
        this._application.put('/question', this._putQuestion.bind(this));
        this._application.post('/point', this._postNewPoint.bind(this));
        this._application.put('/point/:pointId', this._updatePoint.bind(this));
        this._application.delete('/point/:pointId', this._deletePoint.bind(this));
    }

    _putQuestion(request, response) {
        return this._dataService.setQuestionData(request.body).then(data => {
            response.send(data);
        }).catch(err => {
            response.status(500).send(err);
        });
    }

    _getAllPointsAndData(request, response) {
        return this._dataService.getAllPointsAndData().then(data => {
            response.send(data);
        }).catch(err => {
            response.status(500).send(err);
        });
    }

    _postNewPoint(request, response) {
        return this._dataService.createNewPoint(request.body).then(data => {
            response.send(data);
        }).catch(err => {
            response.status(500).send(err);
        });
    }

    _updatePoint(request, response) {
        return this._dataService.updatePoint(request.params.pointId, request.body).then((data) => {
            response.send(data);
        }).catch(err => {
            response.status(500).send(err);
        });
    }

    _deletePoint(request, response) {
        return this._dataService.deletePoint(request.params.pointId).then((data) => {
            response.send(data);
        }).catch(err => {
            response.status(500).send(err);
        });
    }
}

module.exports = RestApplication;