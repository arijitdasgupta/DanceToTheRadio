class ImagesRepository {
    constructor(s3) {
        this._s3 = s3;
    }

    getImage(imageId) {
        return new Promise((resolve, reject) => {
            try {
                this._s3.getObject({
                    Key: imageId, 
                    Bucket: 'Images', 
                },  (err, data) => {
                    if (err) {
                        reject(err);
                    } else {
                        resolve({
                            contentType: data.ContentType,
                            imageBody: data.Body
                        });
                    }
                });
            } catch (err) {
                reject(err);
            }
        });
    }

    postImage(imageId, imageBody, contentType) {
        return new Promise((resolve, reject) => {
            try {
                this._s3.upload({
                    Key: imageId, 
                    Body: imageBody, 
                    Bucket: 'Images', 
                    ContentType: contentType
                },  (err, data) => {
                    if (err) {
                        reject(err);
                    } else {
                        resolve(data);
                    }
                });
            } catch (err) {
                reject(err);
            }
        });
    }

    deleteImage(imageId) {
        return new Promise((resolve, reject) => {
            try {
                this._s3.deleteObject({
                    Key: imageId,
                    Bucket: 'Images'
                }, (err, data) => {
                    if (err) {
                        reject(err);
                    } else {
                        resolve(data);
                    }
                });
            } catch (err) {
                reject(err);
            }
        });
    }
}

module.exports = ImagesRepository;