const ObjectID = require('mongodb').ObjectID;

class DataRepository {
    constructor(mongoConnection) {
        this._mongoConnection = mongoConnection;
    }

    getPoint(pointId) {
        return this._mongoConnection.collection('points').findOne({_id: new ObjectID(pointId)});
    }

    getAllPoints() {
        return this._mongoConnection.collection('points').find({}).sort({
            timestamp: 1
        }).toArray();
    }

    getQuestionData() {
        return this._mongoConnection.collection('question').findOne({});
    }

    setQuestionLabel(questionLabel) {
        return this._mongoConnection.collection('question').updateOne(
            {},
            {
                $set: {
                    label: questionLabel
                }
            },
            {
                upsert: true
            }
        );
    }

    updatePointLabel(pointId, pointLabel) {
        return this._mongoConnection.collection('points')
            .updateOne({_id: new ObjectID(pointId)}, {
                $set: {
                    label: pointLabel
                }
            });
    }

    deletePoint(pointId) {
        return this._mongoConnection.collection('points')
            .deleteOne({_id: new ObjectID(pointId)});
    }

    setImage(pointId) {
        return this._mongoConnection.collection('points')
            .updateOne({_id: new ObjectID(pointId)}, {
                $set: {
                    hasImage: true
                }
            });
    }

    createPoint(pointData) {
        // pointData can't contain ID
        return this._mongoConnection.collection('points').insert(pointData);
    }
}

module.exports = DataRepository;