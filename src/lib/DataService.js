const _ = require('lodash');

class DataService {
    constructor(dataRepository, imagesRepository) {
        this._dataRepository = dataRepository;
        this._imagesRepository = imagesRepository;
    }

    /**
     * Gets all the point and question data
     */
    getAllPointsAndData() {
        return Promise.all([
            this._dataRepository.getAllPoints(),
            this._dataRepository.getQuestionData()
        ]).then(([points, questionData]) => {
            return {
                points,
                questionData
            };
        });
    }

    /**
     * Gets question object
     */
    getQuestionData() {
        return this._dataRepository.getQuestionData();
    }

    /**
     * Sets the question label
     * 
     * @param {object} questionData 
     */
    setQuestionData(questionData) {
        return this._dataRepository.setQuestionLabel(questionData.label);
    }

    /**
     * Creates new point...
     * 
     * @param {object} pointData 
     */
    createNewPoint(pointData) {
        const newPointData = _.extend({
            label: null
        }, pointData, {
            timestamp: Date.now(),
            hasImage: false
        });
        return this._dataRepository.createPoint(newPointData);
    }

    /**
     * Updates point of pointId with new object
     * 
     * @param {string} pointId 
     * @param {object} pointData 
     */
    updatePoint(pointId, pointData) {
        return this._dataRepository.updatePointLabel(pointId, pointData.label);
    }

    /**
     * Checks if point exists, if it does deletes it, or throws error.
     * If point has an image, then deletes the image too.
     * 
     * @param {string} pointId 
     */
    deletePoint(pointId) {
        return this._dataRepository.getPoint(pointId).then((point) => {
            return point ? (point.hasImage ?
                Promise.all([
                    this._imagesRepository.deleteImage(pointId),
                    this._dataRepository.deletePoint(pointId)
                ]) : 
                this._dataRepository.deletePoint(pointId)) :
                Promise.reject(null);
        });
    }
}

module.exports = DataService;