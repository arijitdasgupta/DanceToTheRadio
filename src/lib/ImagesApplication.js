const express = require('express');
const bodyParser = require('body-parser');

class ImagesApplication {
    constructor(imagesService) {
        this._imagesService = imagesService;

        this._application = express();
        this._application.use(bodyParser.raw({
            inflate: true,
            limit: '2048kb',
            type: '*/*'
        }));
        this._application.post('/:imageId', this._postImage.bind(this));
        this._application.get('/:imageId', this._getImage.bind(this));
    }

    _postImage(request, response) {
        this._imagesService.postImage(request.params.imageId, request.body, request.get('Content-Type')).then(() => {
            response.status(200).send('OK');
        }).catch(err => {
            // TODO: Better error status, for 404 and 500s
            response.status(500).send(err);
        });
    }

    _getImage(request, response) {
        this._imagesService.getImage(request.params.imageId).then((imageData) => {
            response.set('Content-Type', imageData.contentType).send(imageData.imageBody);
        }).catch(err => {
            // TODO: Better error status, for 404 and 500s
            response.status(500).send(err);
        });
    }
}

module.exports = ImagesApplication;