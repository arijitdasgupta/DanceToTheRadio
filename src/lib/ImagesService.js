class ImagesService {
    constructor(dataRepository, imagesRepository) {
        this._dataRepository = dataRepository;
        this._imagesRepository = imagesRepository;
    }

    getImage(pointId) {
        return this._dataRepository.getPoint(pointId).then(doc => {
            return doc ? 
                this._imagesRepository.getImage(pointId) :
                Promise.reject(null);
        });
    }

    postImage(pointId, imageBody, contentType) {
        return this._dataRepository.getPoint(pointId).then(doc => {
            return doc ? 
                this._imagesRepository.postImage(pointId, imageBody, contentType)
                    .then(() => {
                        this._dataRepository.setImage(pointId)
                    }) :
                Promise.reject(null);
        });
    }
}

module.exports = ImagesService;