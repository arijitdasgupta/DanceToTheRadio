const express = require('express');
const expressLogger = require('express-logging');
const logger = require('logops');
const path = require('path');

const RestApplication = require('./lib/RestApplication');
const ImagesApplication = require('./lib/ImagesApplication');
const ImagesRepository = require('./lib/ImagesRepository');
const DataRepository = require('./lib/DataRepository');
const DataService = require('./lib/DataService');
const ImagesService = require('./lib/ImagesService');

function createMainApplication (mongoConnection, s3) {
    // Repositories
    const imagesRepository = new ImagesRepository(s3);
    const dataRepository = new DataRepository(mongoConnection);

    // Services
    const dataService = new DataService(dataRepository, imagesRepository);
    const imagesService = new ImagesService(dataRepository, imagesRepository);

    // Sub-applications
    const restApplication = new RestApplication(dataService);
    const imagesApplication = new ImagesApplication(imagesService);

    const application = express();

    application.use(expressLogger(logger));
    application.use('/rest', restApplication._application);
    application.use('/images', imagesApplication._application);

    // Status endpoint for Docker healthchecks etc.
    application.get('/status', (request, response) => {
        response.send('OK');
    });

    // Not the best way, but works...
    application.get('/', (request, response) => response.sendFile(path.join(__dirname, '../index.html')));
    application.get('/*', express.static('build'));

    return application;
}

module.exports = createMainApplication;